package az.ingress.unibootcampsecurity.repository;

import az.ingress.unibootcampsecurity.domain.User;
import java.util.Optional;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    @EntityGraph(type = EntityGraph.EntityGraphType.LOAD, attributePaths = "authorities")
    Optional<User> findByUsername(String username);
}
