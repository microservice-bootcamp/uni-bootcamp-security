package az.ingress.unibootcampsecurity.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1")
public class GreetingsController {

    //For all
//    @CrossOrigin(origins = "http://tofik.com")
    @GetMapping("/hello")U
    public ResponseEntity<String> greetings() {
        return ResponseEntity.ok("Hello");
    }

    //For authenticated users and managers
    @GetMapping("/user")
    public ResponseEntity<String> user() {
        return ResponseEntity.ok("Welcome to the site");
    }

    //For managers
    @PostMapping("/user")
    public ResponseEntity<String> save() {
        return ResponseEntity.ok("Welcome to the site");
    }

    //For managers
    @GetMapping("/manager")
    public ResponseEntity<String> manager() {
        return ResponseEntity.ok("Welcome to the site, manager");
    }

}
