package az.ingress.unibootcampsecurity;

import az.ingress.unibootcampsecurity.config.JwtService;
import az.ingress.unibootcampsecurity.domain.Authority;
import az.ingress.unibootcampsecurity.domain.Role;
import az.ingress.unibootcampsecurity.domain.User;
import az.ingress.unibootcampsecurity.repository.UserRepository;
import io.jsonwebtoken.Claims;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class UniBootcampSecurityApplication implements CommandLineRunner {

    private final UserRepository userRepository;
    private final PasswordEncoder encoder;
    private final JwtService jwtService;

    public static void main(String[] args) {
        SpringApplication.run(UniBootcampSecurityApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Authority a = new Authority();
        a.setRole(Role.USER);
        User user = User.builder()
                .username("Abbas")
                .password(encoder.encode("102030"))
                .isAccountNonExpired(true)
                .isCredentialsNonExpired(true)
                .isEnabled(true)
                .isAccountNonLocked(true)
                .authorities(Set.of(a))
                .build();

        userRepository.save(user);
        log.info("user saved {}" , user);


        String s = jwtService.issueToken(user);
        log.info("token generated {}" , s);

    }
}
