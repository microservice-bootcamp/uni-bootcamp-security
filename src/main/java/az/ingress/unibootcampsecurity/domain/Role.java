package az.ingress.unibootcampsecurity.domain;

public enum Role {

    MANAGER,
    USER
}
